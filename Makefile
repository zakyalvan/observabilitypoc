SERVICE_NAME := Wartek.Otel.Sample.Todo
SERVICE_VERSION := 1.0.0

CREDENTIALS_FILE := $(HOME)/.config/gcloud/application_default_credentials.json

all: prepare build test

.PHONY: prepare
prepare:
	@echo "Use credentials file from : $(CREDENTIALS_FILE)"
	@[ -f $(CREDENTIALS_FILE) ] && cp $(CREDENTIALS_FILE) ./Sources/TodoAPI/.gcloud/credentials.json

.PHONY: build
build:
	@dotnet restore
	@dotnet build --no-restore

.PHONY: test
test:
	@dotnet test --no-restore

.PHONY: run
run:
	@docker-compose up --build -d

.PHONY: stop
stop:
	@docker-compose down -v