using System.Diagnostics;
using Google.Cloud.Diagnostics.AspNetCore3;
using Google.Cloud.Diagnostics.Common;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenTelemetry.Exporter;
using OpenTelemetry.Instrumentation.AspNetCore;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using TodoAPI.Entities;
using TodoAPI.Services;

const string ServiceName = "Todo.API";
const string ServiceVersion = "1.0.0";


var builder = WebApplication.CreateBuilder(args);

// Add DB Context.
builder.Services.AddDbContext<TodoDbContext>(options => options.UseNpgsql(builder.Configuration.GetConnectionString("TodoDatabase")));

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Configure observability components : logging, metrics and tracing.
builder.Services.AddLogging(builder => 
{
    // TODO(zakyalvan): Set project-id in appsettings.
    builder.AddGoogle(new LoggingServiceOptions{
        ProjectId="logging-sink-345804",
        ServiceName=ServiceName,
        Version=ServiceVersion,
        LoggerDiagnosticsOutput=Console.Out
    });
});
builder.Services.AddOpenTelemetryTracing(builder =>
{
    builder
        .AddOtlpExporter(options =>
        {
            options.Endpoint = new Uri("http://todoapi.otel:4317");
            options.Protocol = OtlpExportProtocol.Grpc;
        })
        .AddSource(ServiceName)
        .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName: ServiceName, serviceVersion: ServiceVersion).AddTelemetrySdk())
        //.AddHttpClientInstrumentation()
        .AddAspNetCoreInstrumentation(options =>
        {
            options.Filter = (req) => !req.Request.Path.ToUriComponent().Contains("index.html", StringComparison.OrdinalIgnoreCase)
                && !req.Request.Path.ToUriComponent().Contains("swagger", StringComparison.OrdinalIgnoreCase);
        })
        .AddSqlClientInstrumentation(options =>
        {
            options.SetDbStatementForText = true;
            options.RecordException = true;
        });
        //.AddRedisInstrumentation();
});

builder.Services.AddOpenTelemetryMetrics(builder =>
{
    builder
        .AddHttpClientInstrumentation()
        .AddAspNetCoreInstrumentation()
        .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(ServiceName).AddTelemetrySdk())
        .AddMeter("TodoAPIMeter")
        .AddOtlpExporter(options =>
        {
            options.Endpoint = new Uri("http://todoapi.otel:4317");
        });
});

builder.Services.Configure<AspNetCoreInstrumentationOptions>(options =>
{
    options.RecordException = true;
});

// Add healthcheck probes.
builder.Services.AddHealthChecks()
    .AddNpgSql(services => services.GetRequiredService<IConfiguration>().GetConnectionString("TodoDatabase"))
    .AddRedis(services => "todoapi.redis:6379");

// Register our todo service.
builder.Services.AddTodoService();

var app = builder.Build();

// Configure Swagger
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.RoutePrefix = string.Empty;
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Todo API");

    options.ShowCommonExtensions();
    options.EnableValidator();
    options.DisplayOperationId();
    options.DisplayRequestDuration();

    // to remove try it out
    // options.SupportedSubmitMethods();

    options.EnableDeepLinking();
    options.EnableFilter();
    options.UseRequestInterceptor("(request) => { return request; }");
    options.UseResponseInterceptor("(response) => { return response; }");
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.MapHealthChecks("/health/ready", new HealthCheckOptions
{
    Predicate = _ => true,
    AllowCachingResponses = false,
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});
app.MapHealthChecks("/health/live", new HealthCheckOptions
{
    Predicate = _ => true,
    AllowCachingResponses = false,
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});

app.Run();
