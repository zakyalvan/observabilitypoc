using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TodoAPI.Entities;
using TodoAPI.Services;

namespace TodoAPI.Controllers;

public class TodoController : ControllerBase {
    private readonly ILogger<TodoController> _logger;

    private readonly ITodoService _todos;

    private readonly ActivitySource _activities;

    public TodoController(ILogger<TodoController> logger, ITodoService todos) {
        this._logger = logger;
        this._todos = todos;
    }

    [HttpPost("todos")]
    public async Task<ActionResult<TodoItem>> CreateTodo([FromBody] CreateParameters parameters) {
        using(Activity activity = Activity.Current.Source.CreateActivity("Controller", ActivityKind.Server))
        {
            try 
            {
                _logger.LogInformation("creating a new todo item");
                return await _todos.CreateTodo(parameters);
            }
            catch(Exception e)
            {
                _logger.LogError(e, "error while creating a new todo item");
                throw e;
            }
        }
    }
}