using Microsoft.EntityFrameworkCore;

namespace TodoAPI.Entities;

public class TodoDbContext : DbContext {
    public TodoDbContext(DbContextOptions<TodoDbContext> options) : base(options) 
    {
    }

    public DbSet<UserTodo> Todos {get; set;}
}