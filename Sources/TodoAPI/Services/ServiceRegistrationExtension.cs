using Microsoft.Extensions.DependencyInjection;

namespace TodoAPI.Services;

public static class ServiceRegistrationExtension {
    public static IServiceCollection AddTodoService(this IServiceCollection services) {
        return services.AddScoped<ITodoService, TodoService>();
    }
}