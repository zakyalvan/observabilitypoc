using Microsoft.EntityFrameworkCore;
using TodoAPI.Entities;

namespace TodoAPI.Services;

internal class TodoService : ITodoService
{
    private readonly ILogger<TodoService> _logger;
    private readonly TodoDbContext _database;

    public TodoService(ILogger<TodoService> logger, TodoDbContext database) 
    {
        this._logger = logger;
        this._database = database;
    }

    public Task<TodoItem> CompleteTodo(Guid id)
    {
        throw new NotImplementedException();
    }

    public async Task<TodoItem> CreateTodo(CreateParameters parameters)
    {
        await _database.Todos.AddAsync(new UserTodo{
            Owner=parameters.Owner,
            Description=parameters.Description,
            CreatedTime=DateTime.Now,
            Completed=false
        });

        return await Task.Run<TodoItem>(() => new TodoItem());
    }

    public async Task<TodoItem> GetTodo(Guid id)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<TodoItem>> ListTodos()
    {
        throw new NotImplementedException();
    }

    public Task<TodoItem> UpdateTodo()
    {
        throw new NotImplementedException();
    }
}