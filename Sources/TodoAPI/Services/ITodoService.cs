namespace TodoAPI.Services;

public interface ITodoService {
    public Task<TodoItem> CreateTodo(CreateParameters parameters);

    public Task<TodoItem> GetTodo(Guid id);

    public Task<IEnumerable<TodoItem>> ListTodos();

    public Task<TodoItem> CompleteTodo(Guid id);

    public Task<TodoItem> UpdateTodo();
}