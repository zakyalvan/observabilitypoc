namespace TodoAPI.Services;

public class CreateParameters {
    public string Description {get;set;}
    public string Owner {get;set;}

    public string Priority{get;set;}
}