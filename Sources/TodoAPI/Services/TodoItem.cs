namespace TodoAPI.Services;

public class TodoItem {
    public Guid Id {get; set;}
    
    public string Description {get; set;}

    public string Owner {get; set;}

    public DateTime CreatedTime {get; set;}

    public bool Completed {get; set;}

    public DateTime CompletedTime {get; set;}
}